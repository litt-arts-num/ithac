var Encore = require('@symfony/webpack-encore');

Encore
  .setOutputPath('public/build/')
  .setPublicPath('/build')
  .addEntry('js/main', './assets/js/main.js')
  .addEntry('js/comment', './assets/js/comment.js')
  .addEntry('js/notes', './assets/js/notes.js')
  .addEntry('js/search', './assets/js/search.js')
  .addEntry('js/datatable', './assets/js/datatable.js')
  .addStyleEntry('css/app', './assets/css/app.css')
  .addStyleEntry('css/toastr', './node_modules/toastr/build/toastr.min.css')
  .addStyleEntry('css/fontfaces', './assets/css/fontfaces.css')
  .addStyleEntry('css/style', './assets/css/style.css')
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning()
  .createSharedEntry('js/vendor', './assets/js/vendor.js')
  .autoProvidejQuery()
  .autoProvideVariables({
    Toastr: 'toastr',
    bsCustomFileInput: 'bs-custom-file-input'
  })
  .configureBabelPresetEnv((config) => {
      config.useBuiltIns = 'usage';
      config.corejs = 3;
    })
module.exports = Encore.getWebpackConfig();
