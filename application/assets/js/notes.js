document.addEventListener("DOMContentLoaded", function() {
  noteHandler.init();
});

var noteHandler = {
  bgClass:"note-hover",

  init: function(){
    let notes = document.querySelectorAll("sup[data-type=note]")
    notes.forEach(function(note) {
      note.addEventListener("mouseenter", noteHandler.addBackground, false)
      note.addEventListener("mouseout", noteHandler.removeBackground, false)
    });

    let noteLinks = document.querySelectorAll("a[data-type=note-link]")
    noteLinks.forEach(function(noteLink) {
      noteLink.addEventListener("click", noteHandler.backToNoteCall, false)
    });

    return;
  },

  scrollToEl: function(el) {
    el.scrollIntoView()
    window.scroll(0, window.scrollY - 100)
  },

  backToNoteCall: function(evt){
    evt.preventDefault()

    let noteLink = evt.currentTarget
    let noteId = noteLink.getAttribute('href')
    noteId = noteId.substring(1);
    let note = document.querySelector("[name="+noteId+"]")
    let seg = noteHandler.getSeg(note.parentNode)
    seg.classList.add(noteHandler.bgClass)
    setTimeout(() => {seg.classList.remove(noteHandler.bgClass)}, 4000);
    noteHandler.scrollToEl(seg)

    return;
  },

  addBackground: function(evt){
    let seg = noteHandler.getSeg(evt.currentTarget)
    seg.classList.add(noteHandler.bgClass)

    return;
  },

  removeBackground: function(evt){
    let seg = noteHandler.getSeg(evt.currentTarget)
    seg.classList.remove(noteHandler.bgClass)

    return;
  },

  getSeg: function(note){
    let target = note.dataset.ref

    return seg = document.querySelector("span[data-type=segment][data-id="+target+"]")
  }
}
