import 'jquery-ui/ui/widgets/draggable';
import 'jquery-ui/ui/widgets/resizable';

document.addEventListener("DOMContentLoaded", function() {
  app.init()
  app.addBtns()
  app.testAnchor()
});

var app = {
  paratextContainer: null,
  commentsContainer: null,
  closeBtn: "close-comments",
  selector: "#paratext-container *[id]",
  highlightClass: "bg-warning",
  paratextId: document.querySelector('.js-paratext-id').dataset.id,
  allComments: null,

  testAnchor: function() {
    if (window.location.hash) {
      let hash = window.location.hash.substring(1)
      let el = document.getElementById(hash)
      if (el) {
        el.classList.add(app.highlightClass)
        document.querySelector('[data-target="' + hash + '"]').click()
        app.scrollToEl(el)
      }
    }
  },

  deleteComment: function(event) {
    let btn = event.currentTarget
    let commentContainer = btn.closest('.list-group-item')
    let commentId = btn.dataset.id
    let commentTarget = commentContainer.dataset.commentid
    let route = Routing.generate('message_annotation_remove', {
      id: commentId
    });

    let request = new XMLHttpRequest()
    request.open('GET', route, true)
    request.onload = function() {
      if (this.status >= 200 && this.status < 400) {
        btn.closest('.list-group-item').remove()
        if (commentTarget) {
          document.querySelector(".annotate[data-target-id="+commentTarget+"] .count").innerText = app.countByTarget(commentTarget)
        }
        Toastr.info("commentaire supprimé")
      }
    }
    request.send()
  },

  init: function() {
    let generalComments = document.querySelector("#general-comments")
    generalComments.addEventListener("click", app.displayGeneral, false)
    app.allComments = document.getElementById("comments-container").innerHTML
    app.paratextContainer = document.getElementById("paratext-container")
    app.commentsContainer = document.getElementById("comments-container")
    app.handleCloseBtn()
    window.addEventListener("scroll", app.handleTopOffset, false)
    window.addEventListener("resize", app.handleCommentsHeight, false)
    app.handleCommentsHeight()

    document.addEventListener("keypress", function(event) {
      if (event.keyCode == 13 && !event.shiftKey) {
        event.preventDefault()
        const elem = document.querySelector('textarea#message_content')
        if (elem === document.activeElement) {
          elem.closest('form').submit()
        }
      }
    });

    $( "body" ).on( "click", ".edit-comment", function(e) {
      app.editComment(e)
    });

    const charNum = document.querySelector('[data-char-num]').getAttribute("data-char-num")
    const paratextInfo = document.querySelector("#paratext-info")
    const newContent = paratextInfo.getAttribute("data-content") + " "+ charNum
    paratextInfo.setAttribute("data-content", newContent)
  },

  handleCloseBtn: function() {
    let closeBtn = document.getElementById(app.closeBtn)
    closeBtn.addEventListener("click", app.toggleComments, false)
  },

  scrollToEl: function(el) {
    el.scrollIntoView()
    window.scroll(0, window.scrollY - 100)
  },

  reduceParatext: function() {
    let containerClasses = app.paratextContainer.classList
    if (containerClasses.contains("col-9")) {
      containerClasses.remove("col-9")
      containerClasses.add("col")
    } else {
      containerClasses.remove("col")
      containerClasses.add("col-9")
    }
  },

  displayGeneral: function(){
    let comments = app.commentsContainer
    comments.innerHTML = app.allComments
    app.handleCloseBtn()
    app.handleTopOffset();
    if (comments.classList.contains("d-none")) {
      comments.classList.remove("d-none")
      app.removeHighlight()
      app.reduceParatext()
    }

    let deleteCommentBtns = document.querySelectorAll(".comment-delete")
    for (var btn of deleteCommentBtns) {
      btn.addEventListener("click", function(event){
        app.deleteComment(event)
      })
    }

  },

  toggleComments: function() {
    let comments = app.commentsContainer
    if (comments.classList.contains("d-none")) {
      comments.classList.remove("d-none")
      comments.innerHTML = app.allComments
      app.handleCloseBtn()
      app.handleTopOffset();
    } else {
      comments.classList.add("d-none")
      app.removeHighlight()
    }
    app.reduceParatext()
  },

  handleCommentsHeight: function() {
    app.commentsContainer.style.height = "100vh";
  },

  handleTopOffset: function() {
    let topOffset = app.paratextContainer.getBoundingClientRect().top;
    app.commentsContainer.style.top = (topOffset > 0) ? topOffset + "px" : "0px"
  },

  countByTarget: function(targetId) {
    return document.querySelectorAll('[data-commentId="' + targetId + '"]').length;
  },

  addBtns: function() {
    let els = document.querySelectorAll(app.selector)
    els.forEach(function(el) {
      let count = app.countByTarget(el.id)
      let annotateBtn = document.createElement("span")
      annotateBtn.setAttribute("class", "btn btn-sm btn-link annotate")
      annotateBtn.setAttribute("data-target-id", el.id)
      if (count == 0) {
        annotateBtn.classList.add("text-muted")
      }
      annotateBtn.dataset.target = el.id
      annotateBtn.innerHTML = "<i class='far fa-comment-alt'></i> <span class='count'>" + count + "</span>"
      el.appendChild(annotateBtn)
      annotateBtn.addEventListener("click", app.displayComments, false)
    });
  },

  removeHighlight: function() {
    let highlightedItems = document.querySelectorAll("." + app.highlightClass)

    highlightedItems.forEach(function(item) {
      item.classList.remove(app.highlightClass)
    });
  },

  editComment: function(evt) {
    let btn = evt.currentTarget
    let target = btn.dataset.id
    let data = "target=" + target
    let route = Routing.generate('message_edit', {
      id: target
    });

    document.querySelector('textarea#message_content').closest('.list-group-item').remove()

    let request = new XMLHttpRequest()
    request.open('POST', route, true)
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = function() {
      if (this.status >= 200 && this.status < 400) {
        var resp = this.response
        btn.closest('.list-group-item').innerHTML = resp
      }
    }
    request.send(data)
  },

  displayComments: function(evt) {
    app.removeHighlight()
    let target = evt.currentTarget.dataset.target
    let el = document.getElementById(target)
    el.classList.add(app.highlightClass)

    let data = "target=" + target
    let route = Routing.generate('message_annotations', {
      id: app.paratextId
    });
    let request = new XMLHttpRequest()
    request.open('POST', route, true)
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onload = function() {
      if (this.status >= 200 && this.status < 400) {
        var resp = this.response
        document.getElementById('comments-body').innerHTML = resp
        document.getElementById('comments-title').innerHTML = "Commentaires sur '" + target + "'"

        let comments = app.commentsContainer
        if (comments.classList.contains("d-none")) {
          comments.classList.remove("d-none")
          app.reduceParatext()
        }
        app.handleCloseBtn()
        app.handleTopOffset()
        app.scrollToEl(el)

        let deleteCommentBtns = document.querySelectorAll(".comment-delete")
        for (var btn of deleteCommentBtns) {
          btn.addEventListener("click", function(event){
            app.deleteComment(event)
          })
        }
      }
    };
    request.send(data)
  }
}
