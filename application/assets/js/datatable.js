import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
require('datatables.net-buttons-bs4')();

$(document).ready(function() {
  $('table').DataTable({
    "paging": false,
    "ordering": true,
    "info": false,
    "searching": true,
    language: {
      processing: "Traitement en cours...",
      search: "Filtrer&nbsp;",
      lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
      info: "Affichage de l'élement _START_ &agrave; _END_ sur _TOTAL_ éléments",
      infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
      infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
      infoPostFix: "",
      loadingRecords: "Chargement en cours...",
      zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
      emptyTable: "Aucune donnée disponible dans le tableau",
      aria: {
        sortAscending: ": activer pour trier la colonne par ordre croissant",
        sortDescending: ": activer pour trier la colonne par ordre décroissant"
      }
    }
  });
});
