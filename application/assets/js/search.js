const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const query = urlParams.get('query');

if (query != null) {
  const Mark = require('mark.js');
  const type = urlParams.get('type');
  var instance = new Mark("#paratext-container");

  if (type == "text") {
    instance.mark(query, {
      'separateWordSearch': false,
      'acrossElements': true
    });
  } else if (type == "regexp") {
    var regex = regexFromString(query)
    instance.markRegExp(regex);
  }
  let el = document.querySelector("[data-markjs]")
  if (el) {
    el.scrollIntoView()
  }
}

function regexFromString (string) {
  var match = /^\/(.*)\/([a-z]*)$/.exec(string)
  return new RegExp(match[1], match[2])
}
