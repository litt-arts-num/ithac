<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei sch" version="2.0"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:variable name="STAND-ALONE">0</xsl:variable>
    <!--<xsl:preserve-space elements=""/>-->

    <xsl:template match="/tei:TEI">
        <xsl:if test="$STAND-ALONE = 1">
            <xsl:text disable-output-escaping="yes">&lt;html lang="fr"></xsl:text>
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- Bootstrap CSS -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>

                <title>Manuel généré depuis ODD - <xsl:value-of
                        select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/></title>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body></xsl:text>
        </xsl:if>

        <h1 class="display-4">
            <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
        </h1>
        <div class="row d-block col-12 m-4">
            <xsl:apply-templates select="tei:text/tei:body/*"/>
        </div>
        <hr/>
        <h3>Remarques générales sur le modèle TEI et son manuel</h3>
        <ul>
            <li>
                <xsl:text>Modules et éléments utilisés&nbsp;:</xsl:text>
                <ul>
                    <xsl:apply-templates select="//tei:moduleRef" mode="information"/>
                </ul>
            </li>
            <xsl:if test="count(//tei:classRef) > 0">
                <li>
                    <xsl:text>Classes utilisées&nbsp;:</xsl:text>
                    <ul>
                        <xsl:apply-templates select="//tei:classRef" mode="information"/>
                    </ul>
                </li>
            </xsl:if>
            <xsl:if test="count(//tei:classSpec) > 0">
                <li>
                    <xsl:text>Classes modifiées&nbsp;:</xsl:text>
                    <ul>
                        <xsl:apply-templates select="//tei:classSpec" mode="information"/>
                    </ul>
                </li>
            </xsl:if>
            <xsl:for-each-group select="//tei:elementSpec/@ident" group-by=".">
                <xsl:if test="count(current-group()) > 1">
                    <li class="warning btn-warning btn-sm">
                        <xsl:text>L'élément </xsl:text>
                        <code>
                            <xsl:value-of select="current-grouping-key()"/>
                        </code>
                        <xsl:text> est spécifié plusieurs fois.</xsl:text>
                    </li>
                </xsl:if>
            </xsl:for-each-group>

            <xsl:variable name="tei" select="/tei:TEI"/>
            <xsl:for-each select="//tei:moduleRef/tokenize(@include, ' ')">
                <xsl:if test="not($tei//tei:elementSpec[@ident = current()])">
                    <li class="warning btn-warning btn-sm">
                        <xsl:text>L'élément </xsl:text>
                        <code>
                            <xsl:value-of select="."/>
                        </code>
                        <xsl:text> est utilisé mais pas documenté.</xsl:text>
                    </li>
                </xsl:if>
            </xsl:for-each>
        </ul>
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;/body></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:head">
        <!-- Niveau du titre donné selon la distance à l'élément <body/> -->
        <xsl:variable name="level" select="count(ancestor-or-self::*[ancestor::tei:body]) + 1"/>
        <xsl:element name="h{$level}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:gi">
        <code>
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>&gt;</xsl:text>
        </code>
    </xsl:template>
    <xsl:template match="tei:specGrp">
        <div class="col-12 m-4 color-block">
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="tei:schemaSpec">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:moduleRef"/>
    <xsl:template match="tei:moduleRef" mode="information" name="module_info">
        <li>
            <xsl:text>Module </xsl:text>
            <i>
                <xsl:value-of select="@key"/>
            </i>
            <xsl:if test="@include">
                <xsl:text> (élément</xsl:text>
                <xsl:if test="contains(@include, ' ')">
                    <xsl:text>s</xsl:text>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:for-each select="tokenize(@include, ' ')">
                    <code>
                        <xsl:value-of select="."/>
                    </code>
                    <xsl:choose>
                        <xsl:when test="position() lt last() - 1">
                            <xsl:text>, </xsl:text>
                        </xsl:when>
                        <xsl:when test="position() eq (last() - 1)">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:if>
            <xsl:if test="@exclude">
                <xsl:text> (sauf </xsl:text>
                <xsl:choose>
                    <xsl:when test="contains(@include, ' ')">
                        <xsl:text>les éléments</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>l'élément</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:for-each select="tokenize(@include, ' ')">
                    <code>
                        <xsl:value-of select="."/>
                    </code>
                    <xsl:choose>
                        <xsl:when test="position() lt last() - 1">
                            <xsl:text>, </xsl:text>
                        </xsl:when>
                        <xsl:when test="position() eq (last() - 1)">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="tei:elementSpec">
        <xsl:element name="h{count(ancestor::*[count(ancestor::tei:body) > 0]) + 1}">
            <xsl:text>Élément </xsl:text>
            <code>
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of select="@ident"/>
                <xsl:text>&gt;</xsl:text>
            </code>
        </xsl:element>
        <xsl:if
            test="not(tei:desc) and not(tei:exemplum) and not(tei:remarks) and not(tei:constraintSpec)">
            <div class="warning btn-warning btn-sm">! Cet élément mériterait d'être mieux
                documenté.</div>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:if test="tei:constraintSpec">
            <xsl:text>Contrainte</xsl:text>
            <xsl:if test="count(tei:constraintSpec) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text>&nbsp;:</xsl:text>
            <ul>
                <xsl:apply-templates select="tei:constraintSpec" mode="show-constraints"/>
            </ul>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:exemplum">
        <xsl:text>Exemple d'utilisation&nbsp;:</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:attList">
        <xsl:text>Attribut</xsl:text>
        <xsl:if test="count(tei:attDef) > 1">
            <xsl:text>s</xsl:text>
        </xsl:if>
        <xsl:text>&nbsp;:</xsl:text>
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:attDef">
        <li>
            <code>@<xsl:value-of select="@ident"/></code>&nbsp; <i><xsl:apply-templates
                    select="tei:desc" mode="inline"/></i>
            <xsl:if test="tei:valList">
                <xsl:apply-templates select="tei:valList"/>
            </xsl:if>
            <xsl:apply-templates
                select="child::*[not(name(.) = 'valList') and not(name(.) = 'desc')]"/>
        </li>
    </xsl:template>
    <xsl:template match="tei:datatype">
        <xsl:text> (de type&nbsp;: </xsl:text>
        <a>
            <xsl:attribute name="href">
                <xsl:text>https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/ref-</xsl:text>
                <xsl:value-of select="tei:dataRef/@key"/>
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="tei:dataRef/@key"/>
        </a>
        <xsl:text>)</xsl:text>
    </xsl:template>
    <xsl:template match="tei:desc" mode="inline">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:desc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:att">
        <code>
            <xsl:text>@</xsl:text>
            <xsl:apply-templates/>
        </code>
    </xsl:template>
    <xsl:template match="tei:valList">
        <xsl:choose>
            <xsl:when test="count(tei:valItem) > 6">
                <ul>
                    <li>
                        <xsl:for-each select="tei:valItem">
                            <xsl:apply-templates select="." mode="inline"/>
                            <xsl:choose>
                                <xsl:when test="not(position() = last())">
                                    <xsl:text>, </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>.</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    </li>
                </ul>
            </xsl:when>
            <xsl:otherwise>
                <ul>
                    <xsl:apply-templates/>
                </ul>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:valItem">
        <li>
            <code>
                <xsl:value-of select="@ident"/>
            </code>
            <xsl:if test="tei:desc">
                <xsl:text>&nbsp;: </xsl:text>
                <xsl:apply-templates mode="inline"/>
            </xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="tei:valItem" mode="inline">
        <code>
            <xsl:value-of select="@ident"/>
        </code>
        <xsl:if test="tei:desc">
            <xsl:text>&nbsp;(</xsl:text>
            <xsl:apply-templates mode="inline"/>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:desc" mode="inline">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:eg">
        <pre><xsl:apply-templates/></pre>
    </xsl:template>
    <xsl:template match="tei:remarks">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:p[@rendition = 'header']">
        <xsl:element name="h{count(ancestor::*[count(ancestor::tei:body) > 0]) + 1}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:constraintSpec"/>
    <xsl:template match="tei:constraintSpec" mode="show-constraints">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:constraint">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="sch:name">
        <code>
            <xsl:value-of select="ancestor::tei:elementSpec/@ident"/>
        </code>
    </xsl:template>
    <xsl:template match="tei:content"/>
    <xsl:template match="tei:classRef"/>
    <xsl:template match="tei:classRef" mode="information">
        <li style="display: inline list-item;">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                    <xsl:value-of select="@key"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="@key"/>
            </a>
        </li>
    </xsl:template>
    <xsl:template match="tei:classSpec"/>
    <xsl:template match="tei:classSpec" mode="information">
        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                    <xsl:value-of select="@ident"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="@ident"/>
            </a>
            <xsl:text>&nbsp;: </xsl:text>
            <xsl:for-each-group select="tei:attList/tei:attDef" group-by="@mode">
                <xsl:choose>
                    <xsl:when test="@mode = 'delete'">attributs supprimés&nbsp;(</xsl:when>
                    <xsl:otherwise>@mode '<xsl:value-of select="@mode"/>'&nbsp;(</xsl:otherwise>
                </xsl:choose>
                <xsl:for-each select="current-group()">
                    <xsl:text>@</xsl:text>
                    <xsl:value-of select="@ident"/>
                    <xsl:if test="not(position() = last())">, </xsl:if>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
                <xsl:if test="not(position() = last())">, </xsl:if>
            </xsl:for-each-group>
        </li>
    </xsl:template>

    <!--<xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>-->



    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:text>Élément "</xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:text>" non pris en charge par la transformation XSL.</xsl:text>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
