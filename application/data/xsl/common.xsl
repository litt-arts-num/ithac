<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT Common templates for IThaC project
* @author AnneGF@CNRS
* @date : 2023
**/
-->

<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:template name="get_paratextId">
        <xsl:value-of select="substring-before(/tei:TEI/tei:text/tei:body/tei:head/@xml:id,'_0')"/>
    </xsl:template>

    <xsl:template name="get_source">
        <xsl:choose>
            <xsl:when test="count(/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl) = 0">
                <i class="text-muted">source non renseignée</i>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="get_paratextTitle">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
    </xsl:template>

    <xsl:template name="get_paratextAuthor">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author"/>
    </xsl:template>
    
    <xsl:template name="get_paratextLang">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = /tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/@xml:lang]"/>
    </xsl:template>
    
    <xsl:template name="get_sourcePubDate">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:date/@when"/>
    </xsl:template>
    
    <xsl:template name="get_sourcePubPlace">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:pubPlace"/>
    </xsl:template>
    
    <xsl:template name="get_sourcePublisher">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:publisher"/>
    </xsl:template>
    
    <xsl:template name="get_sourceEditors">
        <xsl:for-each select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:editor">
            <xsl:choose>
                <xsl:when test="position() = 1"/>
                <xsl:when test="position() != last()">
                    <xsl:text>, </xsl:text>
                </xsl:when>
                <xsl:when test="position() = last()">
                    <xsl:text> et </xsl:text>
                </xsl:when>
            </xsl:choose>
            <xsl:value-of select="."/>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="get_sourceTitle">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:title"/>
    </xsl:template>
    
    <xsl:template name="get_sourceAuthor">
        <xsl:value-of select="/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author"/>
    </xsl:template>

    <xsl:template match="tei:bibl" mode="common">
        <xsl:variable name="id">
            <xsl:text>bibl_</xsl:text>
            <xsl:number level="any"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@type = 'nontrouve'">
                <xsl:text>La référence demeure introuvable.</xsl:text>
            </xsl:when>
            <xsl:when
                test="string-length(.) > 0 and count(child::*) > 1 and count(child::*[not(name(.) = 'ref')]) > 1">
                <span id="{$id}">
                    <!-- todo: check with team different cases: paper, no author but editor, etc. -->
                    <xsl:if test="tei:author">
                        <xsl:value-of select="tei:author"/>
                        <xsl:if test="
                            tei:title | tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:title[1], tei:editor[1], tei:publisher, tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:title">
                        <i>
                            <xsl:if test="@xml:lang = 'grc'">
                                <xsl:attribute name="class">
                                    <xsl:text>text-greek</xsl:text>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="tei:title"/>
                        </i>
                        <xsl:if test="
                            tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:editor[1], tei:publisher, tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:editor">
                        <xsl:text>ed. </xsl:text>
                        <xsl:value-of select="tei:editor"/>
                        <xsl:if test="
                            tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:publisher, tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:publisher">
                        <xsl:value-of select="tei:publisher"/>
                        <xsl:if test="
                            tei:pubPlace | tei:date
                            and string-length(concat(tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:pubPlace">
                        <xsl:value-of select="tei:pubPlace"/>
                        <xsl:if test="tei:date and string-length(tei:date) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:date">
                        <xsl:choose>
                            <xsl:when test="count(tei:date/@when) > 0">
                                <span title="{tei:date/@when}">
                                    <xsl:value-of select="tei:date"/>
                                </span>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="tei:date"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:if test="tei:biblScope">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="tei:biblScope"/>
                    </xsl:if>
                    <!--<xsl:text>. </xsl:text>-->
                </span>
            </xsl:when>
            <xsl:otherwise>
                <span id="{$id}">
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates
            select="*[not(contains(' author title editor publisher pubPlace date biblScope ', concat(' ', name(), ' ')))]"
        />
    </xsl:template>
</xsl:stylesheet>
