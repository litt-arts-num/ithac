<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT conversion to ODF for IThaC project
* Status : Very experimental 
* @author AnneGF@CNRS
* @date : 2023
**/
-->

<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <office:document-content xmlns:officeooo="http://openoffice.org/2009/office"
            xmlns:css3t="http://www.w3.org/TR/css3-text/"
            xmlns:grddl="http://www.w3.org/2003/g/data-view#"
            xmlns:xhtml="http://www.w3.org/1999/xhtml"
            xmlns:formx="urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:rpt="http://openoffice.org/2005/report"
            xmlns:dc="http://purl.org/dc/elements/1.1/"
            xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
            xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
            xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
            xmlns:loext="urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0"
            xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
            xmlns:oooc="http://openoffice.org/2004/calc"
            xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
            xmlns:ooow="http://openoffice.org/2004/writer"
            xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
            xmlns:ooo="http://openoffice.org/2004/office"
            xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
            xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
            xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
            xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
            xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2"
            xmlns:calcext="urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0"
            xmlns:tableooo="http://openoffice.org/2009/table"
            xmlns:drawooo="http://openoffice.org/2010/draw"
            xmlns:dom="http://www.w3.org/2001/xml-events"
            xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:math="http://www.w3.org/1998/Math/MathML"
            xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
            xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
            xmlns:xforms="http://www.w3.org/2002/xforms" office:version="1.3">
            <office:scripts/>
            <office:font-face-decls>
                <style:font-face style:name="Liberation Sans"
                    svg:font-family="&apos;Liberation Sans&apos;" style:font-family-generic="swiss"
                    style:font-pitch="variable"/>
                <style:font-face style:name="Liberation Serif"
                    svg:font-family="&apos;Liberation Serif&apos;" style:font-family-generic="roman"
                    style:font-pitch="variable"/>
                <style:font-face style:name="Lohit Devanagari"
                    svg:font-family="&apos;Lohit Devanagari&apos;"/>
                <style:font-face style:name="Lohit Devanagari1"
                    svg:font-family="&apos;Lohit Devanagari&apos;"
                    style:font-family-generic="system" style:font-pitch="variable"/>
                <style:font-face style:name="Noto Sans CJK SC"
                    svg:font-family="&apos;Noto Sans CJK SC&apos;"
                    style:font-family-generic="system" style:font-pitch="variable"/>
                <style:font-face style:name="Noto Serif CJK SC"
                    svg:font-family="&apos;Noto Serif CJK SC&apos;"
                    style:font-family-generic="system" style:font-pitch="variable"/>
            </office:font-face-decls>
            <office:automatic-styles>
                <style:style style:name="Table1" style:family="table">
                    <style:table-properties style:width="17cm" table:align="left"/>
                </style:style>
                <style:style style:name="Table1.A" style:family="table-column">
                    <style:table-column-properties style:column-width="8.012cm"/>
                </style:style>
                <style:style style:name="Table1.B" style:family="table-column">
                    <style:table-column-properties style:column-width="8.989cm"/>
                </style:style>
                <style:style style:name="Table1.A1" style:family="table-cell">
                    <style:table-cell-properties style:vertical-align="middle" fo:padding="0cm"
                        fo:border="none"/>
                </style:style>
                <style:style style:name="P1" style:family="paragraph"
                    style:parent-style-name="Text_20_body">
                    <style:paragraph-properties fo:margin-top="0cm" fo:margin-bottom="0cm"
                        style:contextual-spacing="false"/>
                    <style:text-properties officeooo:paragraph-rsid="001bcb76"/>
                </style:style>
                <style:style style:name="T1" style:family="text">
                    <style:text-properties style:text-position="33% 80%"/>
                </style:style>
                <style:style style:name="T2" style:family="text">
                    <style:text-properties fo:font-style="italic"/>
                </style:style>
                <style:style style:name="T3" style:family="text">
                    <style:text-properties fo:background-color="#ff0000"
                        loext:char-shading-value="0"/>
                </style:style>
                <style:style style:name="T4" style:family="text">
                    <style:text-properties fo:background-color="#ffff00"
                        loext:char-shading-value="0"/>
                </style:style>
            </office:automatic-styles>
            <office:body>
                <office:text>
                    <text:sequence-decls>
                        <text:sequence-decl text:display-outline-level="0" text:name="Illustration"/>
                        <text:sequence-decl text:display-outline-level="0" text:name="Table"/>
                        <text:sequence-decl text:display-outline-level="0" text:name="Text"/>
                        <text:sequence-decl text:display-outline-level="0" text:name="Drawing"/>
                        <text:sequence-decl text:display-outline-level="0" text:name="Figure"/>
                    </text:sequence-decls>
                    <table:table table:name="Table1" table:style-name="Table1">
                        <table:table-column table:style-name="Table1.A"/>
                        <table:table-column table:style-name="Table1.B"/>


                        <xsl:call-template name="table_head"/>
                        <xsl:apply-templates select="//tei:body/tei:ab[@type = 'orig']"/>


                    </table:table>
                </office:text>
            </office:body>
        </office:document-content>
    </xsl:template>

    <xsl:template name="table_head">
        <table:table-row>
            <table:table-cell table:style-name="Table1.A1" office:value-type="string">
                <text:h text:style-name="Heading_20_3" text:outline-level="3">
                    <xsl:apply-templates select="//tei:body/tei:head[@type = 'orig']"/>
                </text:h>
            </table:table-cell>
            <table:table-cell table:style-name="Table1.A1" office:value-type="string">
                <text:h text:style-name="Heading_20_3" text:outline-level="3">
                    <xsl:apply-templates select="//tei:body/tei:head[@type = 'trad']"/>
                    <text:bookmark text:name="Pla1558_Camerarius_p3_1"/>
                </text:h>
            </table:table-cell>
        </table:table-row>
    </xsl:template>

    <xsl:template match="tei:abstract"/>

    <xsl:template match="tei:ab[@type = 'orig']" name="table_row">
        <table:table-row>
            <table:table-cell table:style-name="Table1.A1" office:value-type="string">
                <text:p text:style-name="Table_20_Contents">
                    <xsl:apply-templates/>
                </text:p>
            </table:table-cell>
            <table:table-cell table:style-name="Table1.A1" office:value-type="string">
                <text:p text:style-name="Table_20_Contents">
                    <!-- if not following is tei:ab[@type='trad'] error TODO -->
                    <xsl:choose>
                        <xsl:when
                            test="name(following-sibling::*[1]) = 'ab' and following-sibling::*[1]/@type = 'trad'">
                            <xsl:apply-templates select="following-sibling::*[1]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <text:span text:style-name="T4"> PAS DE TRADUCTION TROUVÉE </text:span>
                        </xsl:otherwise>
                    </xsl:choose>
                </text:p>
            </table:table-cell>
        </table:table-row>
    </xsl:template>

    <xsl:template match="tei:ab | tei:seg | tei:head | tei:persName | tei:placeName | tei:date | tei:said">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:ab/tei:bibl">
        <xsl:variable name="note_num">
            <xsl:number level="any" count="tei:ab/tei:bibl"/>
        </xsl:variable>

        <office:annotation office:name="__Annotation__16288_2755633876">
            <!--<dc:creator>AnneGF via XSLT</dc:creator>
            <dc:date>1111-11-11T11:11:11.111111111</dc:date>-->
            <text:p text:style-name="P2">
                <xsl:apply-templates mode="bibl2text"/>
            </text:p>
        </office:annotation>
    </xsl:template>

    <xsl:template match="tei:bibl" mode="bibl2text">
        <xsl:variable name="id">
            <xsl:text>bibl_</xsl:text>
            <xsl:number level="any"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when
                test="string-length(.) > 0 and count(child::*) > 1 and count(child::*[not(name(.) = 'ref')]) > 1">
                <!-- todo: check with team different cases: paper, no author but editor, etc. -->
                <xsl:if test="tei:author">
                    <xsl:value-of select="tei:author"/>
                    <xsl:if test="
                            tei:title | tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:title, tei:editor, tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:title">
                    <xsl:choose>
                        <xsl:when test="@xml:lang = 'grc'">
                            <xsl:value-of select="tei:title"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <text:span text:style-name="T2">
                                <xsl:value-of select="tei:title"/>
                            </text:span>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="
                            tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:editor, tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:editor">
                    <xsl:text>ed. </xsl:text>
                    <xsl:value-of select="tei:editor"/>
                    <xsl:if test="
                            tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:publisher">
                    <xsl:value-of select="tei:publisher"/>
                    <xsl:if test="
                            tei:pubPlace | tei:date
                            and string-length(concat(tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:pubPlace">
                    <xsl:value-of select="tei:pubPlace"/>
                    <xsl:if test="tei:date and string-length(tei:date) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:date">
                    <xsl:value-of select="tei:date"/>
                </xsl:if>
                <xsl:if test="tei:biblScope">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="tei:biblScope"/>
                </xsl:if>
                <!--<xsl:text>. </xsl:text>-->
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:note" mode="bibl2text">
        <text:line-break/>
        <text:span text:style-name="T2">
            <xsl:apply-templates mode="bibl2text"/>
        </text:span>
    </xsl:template>

    <xsl:template match="tei:l">
        <xsl:apply-templates/>
        <xsl:if test="not(name(following-sibling::*[1]) = 'note')">
            <text:line-break/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:foreign">
        <text:span text:style-name="T2">
            <xsl:apply-templates/>
        </text:span>
    </xsl:template>

    <xsl:template match="tei:foreign[@xml:lang = 'grc'] | tei:title[@xml:lang = 'grc']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:title">
        <text:span text:style-name="T2">
            <xsl:apply-templates/>
        </text:span>
    </xsl:template>

    <xsl:template match="tei:cit">
        <text:span text:style-name="T2">
            <xsl:apply-templates/>
        </text:span>
    </xsl:template>

    <xsl:template match="tei:quote">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:note[not(ancestor::tei:note)]">
        <xsl:variable name="note_num">
            <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
        </xsl:variable>

        <text:span text:style-name="T1">
            <text:note text:id="ftn{$note_num}" text:note-class="footnote">
                <text:note-citation>
                    <xsl:value-of select="$note_num"/>
                </text:note-citation>
                <text:note-body>
                    <text:p text:style-name="Footnote">
                        <xsl:apply-templates/>
                    </text:p>
                </text:note-body>
            </text:note>
        </text:span>
    </xsl:template>

    <xsl:template match="tei:note[ancestor::tei:note]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:bibl">
        <xsl:choose>
            <xsl:when test="@type = 'nontrouve'">
                <text:span text:style-name="T2">La référence demeure introuvable.</text:span>
            </xsl:when>
            <xsl:when
                test="string-length(.) > 0 and count(child::*) > 1 and count(child::*[not(name(.) = 'ref')]) > 1">
                <!-- todo: check with team different cases: paper, no author but editor, etc. -->
                <xsl:if test="tei:author">
                    <xsl:value-of select="tei:author"/>
                    <xsl:if test="
                            tei:title | tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:title[1], tei:editor[1], tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:title">
                    <text:span text:style-name="T2">
                        <xsl:value-of select="tei:title"/>
                    </text:span>
                    <xsl:if test="
                            tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:editor[1], tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:editor">
                    <xsl:text>ed. </xsl:text>
                    <xsl:value-of select="tei:editor"/>
                    <xsl:if test="
                            tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:publisher">
                    <xsl:value-of select="tei:publisher"/>
                    <xsl:if test="
                            tei:pubPlace | tei:date
                            and string-length(concat(tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:pubPlace">
                    <xsl:value-of select="tei:pubPlace"/>
                    <xsl:if test="tei:date and string-length(tei:date) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:date">
                    <xsl:choose>
                        <xsl:when test="count(tei:date/@when) > 0">
                            <span text:style-name="T2">
                                <xsl:value-of select="tei:date"/>
                            </span>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="tei:date"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                <xsl:if test="tei:biblScope">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="tei:biblScope"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <text:span text:style-name="T2">
                    <xsl:apply-templates/>
                </text:span>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:apply-templates
            select="*[not(contains(' author title editor publisher pubPlace date biblScope ', concat(' ', name(), ' ')))]"
        />
    </xsl:template>

    <xsl:template match="tei:ref|tei:ptr">
        <xsl:text disable-output-escaping="yes">&lt;text:a xlink:type="simple" xlink:href="</xsl:text>
        <xsl:value-of select="@target"/>
        <xsl:text disable-output-escaping="yes">" text:style-name="Internet_20_link" text:visited-style-name="Visited_20_Internet_20_Link"></xsl:text>
        <xsl:choose>
            <xsl:when test="not(normalize-space(.) = '')">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@target"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text disable-output-escaping="yes">&lt;/text:a></xsl:text>
    </xsl:template>
    <xsl:template match="tei:*">
        <text:span text:style-name="T4">
            <xsl:value-of select="name(.)"/>
        </text:span>
    </xsl:template>

</xsl:stylesheet>
