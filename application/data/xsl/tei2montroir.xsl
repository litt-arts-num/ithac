<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for transformation to html public view
* @author AnneGF@CNRS
* @date : 2023
* Usage: change variable STAND-ALONE to 0 if the manual is to be inclded in a pre-existing webpage
**/
-->

<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:param name="STAND-ALONE" select="false()"/>
    <xsl:include href="common.xsl"/>

    <xsl:template match="/tei:TEI">
        <xsl:if test="$STAND-ALONE = 1">
            <xsl:text disable-output-escaping="yes">&lt;html lang="fr"></xsl:text>
            <head>
                <meta name="viewport"
                    content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, shrink-to-fit=no"/>
                <meta charset="utf-8"/>
                <title>IThAC Montroir - <xsl:value-of
                        select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/></title>
                <link rel="stylesheet"
                    href="http://localhost/ithac-visualisations/commons/css/fontfaces.css"/>
                <link rel="stylesheet"
                    href="http://localhost/ithac-visualisations/detail/assets/css/style.css"/>
                <script type="text/javascript" src="http://localhost/ithac-visualisations/commons/js/settings.js?v=2" defer=""/>
                <script type="text/javascript" src="http://localhost/ithac-visualisations/commons/js/functions.js?v=2" defer=""/>
                <script type="text/javascript" src="http://localhost/ithac-visualisations/assets/js/montroir.js?v=0" defer=""/>
                <!-- Bootstrap CSS -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body></xsl:text>
            <header>
                <h1>
                    <a href="../">IThaC</a>
                </h1>
                <h2>Détail d’un paratexte</h2>
            </header>
        </xsl:if>
        <!--<link rel="stylesheet"
            href="http://localhost/ithac-visualisations/commons/css/fontfaces.css"/>
        <link rel="stylesheet"
            href="http://localhost/ithac-visualisations/detail/assets/css/style.css"/>-->
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"/>
        <div id="wrapper">
            <aside id="metadata">
                <p>
                    <span class="definition">Source</span>
                    <span class="value">
                        <xsl:call-template name="get_source"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Titre du paratexte</span>
                    <span class="value">
                        <xsl:call-template name="get_paratextTitle"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Auteur du paratexte</span>
                    <span class="value">
                        <xsl:call-template name="get_paratextAuthor"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Langue du paratexte</span>
                    <span class="value">
                        <xsl:call-template name="get_paratextLang"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Date</span>
                    <span class="value">
                        <xsl:call-template name="get_sourcePubDate"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Lieu de publication</span>
                    <span class="value">
                        <xsl:call-template name="get_sourcePubPlace"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Imprimeur et/ou libraire</span>
                    <span class="value">
                        <xsl:call-template name="get_sourcePublisher"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Éditeur(s) scientifique(s)/Traducteur(s)</span>
                    <span class="value">
                        <xsl:call-template name="get_sourceEditors"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Titre de l’œuvre</span>
                    <span class="value">
                        <xsl:call-template name="get_sourceTitle"/>
                    </span>
                </p>
                <p>
                    <span class="definition">Auteur</span>
                    <span class="value">
                        <xsl:call-template name="get_sourceAuthor"/>
                    </span>
                </p>
                <xsl:variable name="paratextId">
                    <xsl:call-template name="get_paratextId"/>
                </xsl:variable>
                <div style="display:none;">
                    <a class="montroir-link" href="/admin/paratext/r/{$paratextId}" target="_blank"
                        >Voir sur le Pensoir <sup>(afficher uniquement si connecté)</sup></a>
                    <a class="montroir-link" href="/upload/tei/{$paratextId}.xml" target="_blank"
                        >Télécharger</a>
                </div>
            </aside>
            <main>
                <xsl:apply-templates select="/tei:TEI/tei:text/tei:body"/>
                <hr/>
                <xsl:apply-templates mode="note"
                    select="/tei:TEI/tei:text//tei:note[not(ancestor::tei:note)]"/>
            </main>
        </div>

        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;/body></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:body">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:head">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>
    <xsl:template match="tei:editor">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:pubPlace">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:publisher">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:date">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:ab[@type = 'orig']">
        <p type="orig">
            <xsl:attribute name="xml:id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:ab[@type = 'trad']">
        <p corresp="{@corresp}" type="trad" ana="{@ana}">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:ab">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:persName">
        <span class="persName" ref="{@ref}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:seg">
        <span class="seg" type="{@type}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:note[not(ancestor::tei:note)]">
        <xsl:variable name="num">
            <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
        </xsl:variable>
        <sup data-type="note" data-ref="seg_{$num}">
            <a name="{concat('c',$num)}" href="{concat('#n',$num)}">
                <xsl:value-of select="$num"/>
            </a>
        </sup>
    </xsl:template>
    
    <xsl:template match="tei:note[not(ancestor::tei:note)]" mode="note">
        <xsl:variable name="num">
            <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
        </xsl:variable>
        <div class="note">
            <a name="{concat('n',$num)}" href="{concat('#c',$num)}"><xsl:value-of select="$num"
            />.</a>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="tei:note[ancestor::tei:note]">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!--<xsl:template match="tei:note">
        <span class="note">
            <xsl:apply-templates/>
        </span>
    </xsl:template>-->
    <xsl:template match="tei:bibl">
        <bibl resp="{@resp}">
            <xsl:variable name="bibl">
                <xsl:apply-templates select="." mode="common"/>
            </xsl:variable>
            <xsl:value-of select="normalize-space($bibl)"/>
        </bibl>
    </xsl:template>
    <xsl:template match="tei:author">
        <author ref="{@ref}">
            <xsl:apply-templates/>
        </author>
    </xsl:template>
    <xsl:template match="tei:title">
        <span class="title" ref="{@ref}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:biblScope">
        <biblscope>
            <xsl:apply-templates/>
        </biblscope>
    </xsl:template>
    <xsl:template match="tei:ref">
        <ref target="{@target}">
            <xsl:apply-templates/>
        </ref>
    </xsl:template>
    <xsl:template match="tei:cit">
        <cit type="{@type}">
            <xsl:apply-templates/>
        </cit>
    </xsl:template>
    <xsl:template match="tei:quote">
        <quote>
            <xsl:apply-templates/>
        </quote>
    </xsl:template>
    <xsl:template match="tei:foreign">
        <span class="foreign" xml:lang="{@xml:lang}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="tei:l">
        <l style="display:block;">
            <xsl:apply-templates/>
        </l>
    </xsl:template>
    <xsl:template match="todo"> </xsl:template>

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->

</xsl:stylesheet>
