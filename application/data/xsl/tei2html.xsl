<?xml version="1.0" encoding="UTF-8"?>

<!-- Idée : avoir un résultat du style : http://idt.huma-num.fr/notice.php?id=171 -->
<!-- Données TEI pour test file:/home/annegf/Nextcloud/IThAC/TEI/paratextes/TEI%20Aristophane/Ar1586_Frischlinus_p6.xml et autres -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="1.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:preserve-space
        elements="tei:ab tei:bibl tei:note tei:title tei:seg tei:head tei:persName tei:l"/>

    <xsl:template match="/">
        <div data-char-num="(~{string-length(normalize-space(tei:TEI/tei:text/tei:body))} car.)">
            <h2>Notice :</h2>
            <p id="source">
                <span class="font-weight-bold">Source : </span>
                <xsl:choose>
                    <xsl:when
                        test="count(tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl) = 0">
                        <i class="text-muted">source non renseignée</i>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl"/>
                    </xsl:otherwise>
                </xsl:choose>
            </p>
            <p id="presentation">
                <span class="font-weight-bold">Présentation : </span>
                <xsl:choose>
                    <xsl:when test="
                            count(tei:TEI/tei:teiHeader/tei:profileDesc/tei:abstract) = 0
                            or string-length(tei:TEI/tei:teiHeader/tei:profileDesc/tei:abstract) = 0">
                        <i class="text-muted">résumé non renseigné</i>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:profileDesc/tei:abstract"/>
                    </xsl:otherwise>
                </xsl:choose>
            </p>
            <p id="bibliographie">
                <span class="font-weight-bold">Bibliographie : </span>
                <xsl:choose>
                    <xsl:when
                        test="count(tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl/tei:bibl) > 1">
                        <ul>
                            <xsl:apply-templates
                                select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl/tei:bibl"
                                mode="list"/>
                        </ul>
                    </xsl:when>
                    <xsl:when
                        test="count(tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl/tei:bibl) = 0">
                        <i class="text-muted">bibliographie non renseignée</i>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl/tei:bibl"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </p>
            <p id="translation">
                <span class="font-weight-bold">Traduction : </span>
                <xsl:choose>
                    <xsl:when
                        test="count(tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt[tei:resp = 'Traduction']) > 0">
                        <xsl:for-each
                            select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt[tei:resp = 'Traduction']/tei:name">
                            <xsl:choose>
                                <xsl:when test="position() = last() and not(position() = 1)">
                                    <xsl:text> et </xsl:text>
                                </xsl:when>
                                <xsl:when test="position() > 1">, </xsl:when>
                            </xsl:choose>
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when
                        test="count(tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:respStmt[tei:resp = 'Traduction']) = 0">
                        <i class="text-muted">traduction non renseignée.</i>
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Indiquer au moins un &lt;respStmt/> dont le sous-élément &lt;resp/> est "Traduction"</xsl:text>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates
                            select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl/tei:bibl"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </p>

            <table style="border: 1px solid lightgrey; padding: 3px;">
                <!-- <tr>
                <td style="text-align:center;"><h2>Texte</h2></td>
                <td></td>
                <td style="text-align:center;"><h2>Traduction</h2></td>
            </tr> -->
                <xsl:for-each select="tei:TEI/tei:text/tei:body/*[@type = 'orig']">
                    <xsl:variable name="current_id" select="@xml:id"/>
                    <tr id="{$current_id}">
                        <td style="width:40%; vertical-align: top; padding: 3px;">
                            <xsl:apply-templates select="."/>
                        </td>
                        <td style="width: 10%;"/>

                        <td style="width:40%; vertical-align: top; padding: 3px;">
                            <xsl:apply-templates
                                select="../*[@type = 'trad' and @corresp = concat('#', $current_id)]"
                            />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
            <hr/>
            <xsl:apply-templates mode="note"
                select="/tei:TEI/tei:text//tei:note[not(ancestor::tei:note)]"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:abstract">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:name">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:ab/tei:bibl">
        <sup>
            <xsl:attribute name="title">
                <xsl:call-template name="bibl2text"/>
            </xsl:attribute> (B) </sup>
    </xsl:template>

    <xsl:template match="tei:bibl">
        <xsl:variable name="id">
            <xsl:text>bibl_</xsl:text>
            <xsl:number level="any"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@type = 'nontrouve'">
                <xsl:text>La référence demeure introuvable.</xsl:text>
            </xsl:when>
            <xsl:when
                test="string-length(.) > 0 and count(child::*) > 1 and count(child::*[not(name(.) = 'ref')]) > 1">
                <span id="{$id}">
                    <!-- todo: check with team different cases: paper, no author but editor, etc. -->
                    <xsl:if test="tei:author">
                        <xsl:value-of select="tei:author"/>
                        <xsl:if test="
                                tei:title | tei:editor | tei:publisher | tei:pubPlace | tei:date
                                and string-length(concat(tei:title, tei:editor, tei:publisher, tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:title">
                        <i>
                            <xsl:if test="@xml:lang = 'grc'">
                                <xsl:attribute name="class">
                                    <xsl:text>text-greek</xsl:text>
                                </xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="tei:title"/>
                        </i>
                        <xsl:if test="
                                tei:editor | tei:publisher | tei:pubPlace | tei:date
                                and string-length(concat(tei:editor, tei:publisher, tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:editor">
                        <xsl:text>ed. </xsl:text>
                        <xsl:value-of select="tei:editor"/>
                        <xsl:if test="
                                tei:publisher | tei:pubPlace | tei:date
                                and string-length(concat(tei:publisher, tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:publisher">
                        <xsl:value-of select="tei:publisher"/>
                        <xsl:if test="
                                tei:pubPlace | tei:date
                                and string-length(concat(tei:pubPlace, tei:date)) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:pubPlace">
                        <xsl:value-of select="tei:pubPlace"/>
                        <xsl:if test="tei:date and string-length(tei:date) > 0">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="tei:date">
                        <xsl:choose>
                            <xsl:when test="count(tei:date/@when) > 0">
                                <span title="{tei:date/@when}">
                                    <xsl:value-of select="tei:date"/>
                                </span>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="tei:date"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <xsl:if test="tei:biblScope">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="tei:biblScope"/>
                    </xsl:if>
                    <!--<xsl:text>. </xsl:text>-->
                </span>
            </xsl:when>
            <xsl:otherwise>
                <span id="{$id}">
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates
            select="*[not(contains(' author title editor publisher pubPlace date biblScope ', concat(' ', name(), ' ')))]"
        />
    </xsl:template>

    <xsl:template match="tei:bibl" mode="list">
        <xsl:if test="string-length(.) > 0">
            <li>
                <xsl:apply-templates select="."/>
            </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:bibl" mode="htmltitle" name="bibl2text">
        <xsl:variable name="id">
            <xsl:text>bibl_</xsl:text>
            <xsl:number level="any"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when
                test="string-length(.) > 0 and count(child::*) > 1 and count(child::*[not(name(.) = 'ref')]) > 1">
                <!-- todo: check with team different cases: paper, no author but editor, etc. -->
                <xsl:if test="tei:author">
                    <xsl:value-of select="tei:author"/>
                    <xsl:if test="
                            tei:title | tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:title, tei:editor, tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:title">
                    <i>
                        <xsl:if test="@xml:lang = 'grc'">
                            <xsl:attribute name="class">
                                <xsl:text>text-greek</xsl:text>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="tei:title"/>
                    </i>
                    <xsl:if test="
                            tei:editor | tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:editor, tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:editor">
                    <xsl:text>ed. </xsl:text>
                    <xsl:value-of select="tei:editor"/>
                    <xsl:if test="
                            tei:publisher | tei:pubPlace | tei:date
                            and string-length(concat(tei:publisher, tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:publisher">
                    <xsl:value-of select="tei:publisher"/>
                    <xsl:if test="
                            tei:pubPlace | tei:date
                            and string-length(concat(tei:pubPlace, tei:date)) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:pubPlace">
                    <xsl:value-of select="tei:pubPlace"/>
                    <xsl:if test="tei:date and string-length(tei:date) > 0">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="tei:date">
                    <xsl:choose>
                        <xsl:when test="count(tei:date/@when) > 0">
                            <span title="{tei:date/@when}">
                                <xsl:value-of select="tei:date"/>
                            </span>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="tei:date"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
                <xsl:if test="tei:biblScope">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="tei:biblScope"/>
                </xsl:if>
                <!--<xsl:text>. </xsl:text>-->
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:head">
        <h3>
            <xsl:apply-templates/>
        </h3>
    </xsl:template>

    <xsl:template match="tei:l">
        <xsl:apply-templates/>
        <xsl:if test="not(name(following-sibling::*[1]) = 'note')">
            <br/>
        </xsl:if>
    </xsl:template>

    <!-- Foreign specific case: if lang is greek, do not use italic -->
    <xsl:template match="tei:foreign[@xml:lang = 'grc']">
        <span>
            <xsl:attribute name="class">
                <xsl:text>text-greek</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:text>Terme ou passage en </xsl:text>
                <xsl:value-of
                    select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]"
                />
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:foreign">
        <i>
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="@xml:lang and @xml:lang != ''">
                        <xsl:choose>
                            <xsl:when
                                test="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]">
                                <xsl:text>Terme ou passage en </xsl:text>
                                <xsl:value-of
                                    select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]"
                                />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>Terme étranger, emprunt (</xsl:text>
                                <xsl:value-of select="@xml:lang"/>
                                <xsl:text>)</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Terme étranger, emprunt (langue non spécifiée)</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:if test="not(@xml:lang)">
                <sup>
                    <i class="fa fa-info text-warning" title="@xml:lang manquant"/>
                </sup>
            </xsl:if>
            <xsl:for-each select="@*">
                <xsl:choose>
                    <xsl:when test="name(.) = 'xml:lang' and . = ''">
                        <sup>
                            <i class="fa fa-info text-warning" title="Attribut @xml:lang vide"/>
                        </sup>
                    </xsl:when>
                    <xsl:when test="name() = 'instant' or name() = 'full'"/>
                    <xsl:when
                        test="count(/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()]) = 0">
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "foreign" - Langue spécifiée dans l'attribut @xml:lang "</xsl:text>
                                    <xsl:value-of select="."/>
                                    <xsl:text>" non déclarée. Utiliser l'une des valeurs déclarées (</xsl:text>
                                    <xsl:for-each
                                        select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language">
                                        <xsl:text>"</xsl:text>
                                        <xsl:value-of select="@ident"/>
                                        <xsl:text>"</xsl:text>
                                        <xsl:text> pour le </xsl:text>
                                        <xsl:value-of select="."/>
                                        <xsl:if test="not(position() = last())">
                                            <xsl:text>, </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    <xsl:text>) ou bien déclarer une nouvelle langue dans `tei:langUsage`."</xsl:text>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:when>
                    <xsl:when test="name(.) = 'xml:lang'"/>
                    <xsl:otherwise>
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "foreign" - Attribut inattendu : @</xsl:text>
                                    <xsl:value-of select="name(.)"/>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </i>

    </xsl:template>

    <xsl:template match="tei:title">
        <i>
            <xsl:if test="@xml:lang = 'grc'">
                <xsl:attribute name="class">
                    <xsl:text>text-greek</xsl:text>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:seg">
        <xsl:choose>
            <xsl:when test="@type = 'note'">
                <!-- à terme, à supprimer -->
                <span data-type="segment" data-id="seg_{@n}">
                    <xsl:apply-templates/>
                    <span class="text-warning float-right">
                        <i class="fa fa-exclamation-circle"
                            title="Attention : encodage de note probablement erroné"/>
                    </span>
                </span>
            </xsl:when>
            <xsl:when test="count(./tei:note) > 0">
                <xsl:variable name="num">
                    <xsl:for-each select="tei:note[not(ancestor::tei:note)]">
                        <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
                    </xsl:for-each>
                </xsl:variable>
                <span data-type="segment" data-id="seg_{$num}">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <xsl:otherwise>
                <span data-type="segment">
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="@rend = 'italic'">
                <i>
                    <xsl:apply-templates/>
                </i>
            </xsl:when>
            <xsl:when test="@rend = 'sup'">
                <sup>
                    <xsl:apply-templates/>
                </sup>
            </xsl:when>
            <xsl:otherwise>
                <span style="background-color: yellow;">
                    <xsl:apply-templates/>
                </span>
                <sup>
                    <i class="fa fa-info text-warning"
                        title="Valeur de @rend inattendu ('italic' et 'sup' sont les seules valeurs prévues actuellement)"
                    />
                </sup>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="not(@rend) or @rend = ''">
            <sup>
                <i class="fa fa-info text-warning" title="@rend manquant"/>
            </sup>
        </xsl:if>
        <xsl:for-each select="@*">
            <xsl:choose>
                <xsl:when test="name(.) = 'rend' and . = ''">
                    <sup>
                        <i class="fa fa-info text-warning" title="Attribut @rend vide"/>
                    </sup>
                </xsl:when>
                <xsl:when test="name() = 'rend' and . != ''"/>
                <xsl:when test="name() = 'instant' or name() = 'full'"/>
                <xsl:when test=". != 'italic'">
                    <sup>
                        <i class="fa fa-info text-warning"
                            title="Valeur de @rend non traitée par le XSL"/>
                    </sup>
                </xsl:when>
                <xsl:otherwise>
                    <sup>
                        <i class="fa fa-info text-warning">
                            <xsl:attribute name="title">
                                <xsl:text>Élément "hi" - Attribut inattendu : @</xsl:text>
                                <xsl:value-of select="name(.)"/>
                            </xsl:attribute>
                        </i>
                    </sup>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="tei:ab">
        <xsl:choose>
            <xsl:when test="count(./tei:note) > 0">
                <xsl:variable name="num">
                    <xsl:for-each select="tei:note[not(ancestor::tei:note)]">
                        <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
                    </xsl:for-each>
                </xsl:variable>
                <span data-type="segment" data-id="seg_{$num}">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:note[not(ancestor::tei:note)]">
        <xsl:variable name="num">
            <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
        </xsl:variable>
        <sup data-type="note" data-ref="seg_{$num}">
            <a name="{concat('c',$num)}" href="{concat('#n',$num)}">
                <xsl:value-of select="$num"/>
            </a>
        </sup>
        <xsl:if test="name(preceding-sibling::*[1]) = 'l'">
            <br/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:note[not(ancestor::tei:note)]" mode="note">
        <xsl:variable name="num">
            <xsl:number level="any" count="tei:note[not(ancestor::tei:note)]"/>
        </xsl:variable>
        <div class="note">
            <a name="{concat('n',$num)}" href="{concat('#c',$num)}"><xsl:value-of select="$num"
                />.</a>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:note[ancestor::tei:note]">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:persName">
        <span onMouseOver="this.style.color='grey'" onMouseOut="this.style.color=''"
            title="Nom de personne">
            <xsl:attribute name="title">
                <xsl:text>Nom de personne (</xsl:text>
                <xsl:choose>
                    <xsl:when test="@ref and @ref != ''">
                        <xsl:value-of select="@ref"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Élément "persName" - Attribut @ref manquant ou vide</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>)</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:if test="not(@ref)">
                <sup>
                    <i class="fa fa-info text-warning">
                        <xsl:attribute name="title">
                            <xsl:text>Élément "persName" - Attribut @ref manquant</xsl:text>
                        </xsl:attribute>
                    </i>
                </sup>
            </xsl:if>
            <xsl:for-each select="@*">
                <xsl:choose>
                    <xsl:when test="name(.) = 'ref' and . = ''">
                        <sup>
                            <i class="fa fa-info text-warning" title="Attribut @ref vide"/>
                        </sup>
                    </xsl:when>
                    <xsl:when test="name() = 'instant' or name() = 'full'"/>
                    <xsl:when test="name(.) = 'ref'"/>
                    <xsl:otherwise>
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "persName" - Attribut inattendu : @</xsl:text>
                                    <xsl:value-of select="name(.)"/>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </span>
    </xsl:template>

    <xsl:template match="tei:placeName">
        <span onMouseOver="this.style.color='grey'" onMouseOut="this.style.color=''"
            title="Nom de lieu">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:date">
        <span onMouseOver="this.style.color='grey'" onMouseOut="this.style.color=''" title="Date">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:quote">
        <span>
            <xsl:attribute name="class">
                <xsl:text>quote</xsl:text>
                <xsl:if test="@type">
                    <xsl:text> quote-</xsl:text>
                    <xsl:value-of select="@type"/>
                </xsl:if>
                <xsl:if test="@xml:lang = 'grc'">
                    <xsl:text> text-greek</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="@xml:lang and @xml:lang != ''">
                        <xsl:choose>
                            <xsl:when
                                test="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]">
                                <xsl:text>Citation en </xsl:text>
                                <xsl:value-of
                                    select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]"
                                />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>Citation en langue étrangère (</xsl:text>
                                <xsl:value-of select="@xml:lang"/>
                                <xsl:text>)</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Citation</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:for-each select="@*">
                <xsl:choose>
                    <xsl:when test="name(.) = 'xml:lang' and . = ''">
                        <sup>
                            <i class="fa fa-info text-warning" title="Attribut @xml:lang vide"/>
                        </sup>
                    </xsl:when>
                    <xsl:when test="name() = 'instant' or name() = 'full'"/>
                    <xsl:when
                        test="count(/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()]) = 0">
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "quote" - Langue spécifiée dans l'attribut @xml:lang "</xsl:text>
                                    <xsl:value-of select="."/>
                                    <xsl:text>" non déclarée. Utiliser l'une des valeurs déclarées (</xsl:text>
                                    <xsl:for-each
                                        select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language">
                                        <xsl:text>"</xsl:text>
                                        <xsl:value-of select="@ident"/>
                                        <xsl:text>"</xsl:text>
                                        <xsl:text> pour le </xsl:text>
                                        <xsl:value-of select="."/>
                                        <xsl:if test="not(position() = last())">
                                            <xsl:text>, </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    <xsl:text>) ou bien déclarer une nouvelle langue dans `tei:langUsage`."</xsl:text>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:when>
                    <xsl:when test="name() = 'xml:lang'"/>
                    <xsl:otherwise>
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "quote" - Attribut inattendu : @</xsl:text>
                                    <xsl:value-of select="name(.)"/>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </span>
    </xsl:template>

    <xsl:template match="tei:cit">
        <span>
            <xsl:attribute name="class">
                <xsl:text>cit</xsl:text>
                <xsl:if test="@type">
                    <xsl:text> cit-</xsl:text>
                    <xsl:value-of select="@type"/>
                </xsl:if>
                <xsl:if test="@xml:lang = 'grc'">
                    <xsl:text> text-greek</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <xsl:apply-templates select="tei:quote"/>
            <xsl:if test="tei:note">
                <xsl:apply-templates select="tei:note"/>
            </xsl:if>
            <xsl:for-each select="@*">
                <xsl:choose>
                    <xsl:when test="name(.) = 'xml:lang' and . = ''">
                        <sup>
                            <i class="fa fa-info text-warning" title="Attribut @xml:lang vide"/>
                        </sup>
                    </xsl:when>
                    <xsl:when
                        test="name() = 'type' and (. = '' or not(contains(' refauteur, hyperref, nonref, refœuvre ', .)))">
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Valeur de l'attribut @"</xsl:text>
                                    <xsl:value-of select="name(.)"/>
                                    <xsl:text>" innatendu : "</xsl:text>
                                    <xsl:value-of select="."/>
                                    <xsl:text>"</xsl:text>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:when>
                    <xsl:when
                        test="name() = 'instant' or name() = 'full' or name() = 'xml:lang' or name() = 'type'"/>
                    <xsl:when
                        test="name(.) = 'xml:lang' and count(/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language[@ident = current()/@xml:lang]) = 0">
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "cit" - Langue spécifiée dans l'attribut @xml:lang "</xsl:text>
                                    <xsl:value-of select="."/>
                                    <xsl:text>" non déclarée. Utiliser l'une des valeurs déclarées (</xsl:text>
                                    <xsl:for-each
                                        select="/tei:TEI/tei:teiHeader/tei:profileDesc/tei:langUsage/tei:language">
                                        <xsl:text>"</xsl:text>
                                        <xsl:value-of select="@ident"/>
                                        <xsl:text>"</xsl:text>
                                        <xsl:text> pour le </xsl:text>
                                        <xsl:value-of select="."/>
                                        <xsl:if test="not(position() = last())">
                                            <xsl:text>, </xsl:text>
                                        </xsl:if>
                                    </xsl:for-each>
                                    <xsl:text>) ou bien déclarer une nouvelle langue dans `tei:langUsage`."</xsl:text>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:when>
                    <xsl:otherwise>
                        <sup>
                            <i class="fa fa-info text-warning">
                                <xsl:attribute name="title">
                                    <xsl:text>Élément "cit" - Attribut @</xsl:text>
                                    <xsl:value-of select="name(.)"/>
                                    <xsl:text> inattendu </xsl:text>
                                    <xsl:text> (valeur "</xsl:text>
                                    <xsl:value-of select="."/>
                                    <xsl:text>")</xsl:text>
                                </xsl:attribute>
                            </i>
                        </sup>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </span>
    </xsl:template>

    <!-- Do not do nothing specific for said -->
    <xsl:template match="tei:said">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:list">
        <sup>
            <i class="fa fa-info text-warning">
                <xsl:attribute name="title">
                    <xsl:text>Élément "</xsl:text>
                    <xsl:value-of select="name(.)"/>
                    <xsl:text>" absent de la fiche modèle</xsl:text>
                </xsl:attribute>
            </i>
        </sup>
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
            <sup>
                <i class="fa fa-info text-warning">
                    <xsl:attribute name="title">
                        <xsl:text>Élément "</xsl:text>
                        <xsl:value-of select="name(.)"/>
                        <xsl:text>" absent de la fiche modèle</xsl:text>
                    </xsl:attribute>
                </i>
            </sup>
        </li>
    </xsl:template>

    <xsl:template match="tei:p">
        <xsl:choose>
            <xsl:when test=". = ''">
                <sup>
                    <i class="fa fa-info text-warning">
                        <xsl:attribute name="title">
                            <xsl:text>Élément "</xsl:text>
                            <xsl:value-of select="name(.)"/>
                            <xsl:text>" vide</xsl:text>
                        </xsl:attribute>
                    </i>
                </sup>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:ptr | tei:ref">
        <xsl:choose>
            <xsl:when test="(not(@target) or @target = '')">
                <span class="btn-warning btn-sm">
                    <xsl:attribute name="title">
                        <xsl:text>Élément "</xsl:text>
                        <xsl:value-of select="name()"/>
                        <xsl:text>" - Attribut @target non renseigné</xsl:text>
                    </xsl:attribute>
                    <xsl:text>ref</xsl:text>
                </span>
            </xsl:when>
            <xsl:when test="not(normalize-space(.) = '')">
                <xsl:choose>
                    <xsl:when test="starts-with(@target, 'http')">
                        <a target="_blank" href="{@target}">
                            <xsl:apply-templates/>
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a href="{@target}">
                            <xsl:apply-templates/>
                        </a>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="starts-with(@target, 'http')">
                        <a target="_blank" href="{@target}">
                            <i class="fas fa-link ml-1"/>
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <a href="{@target}">
                            <i class="fas fa-link ml-1"/>
                        </a>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!--<xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>-->

    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
