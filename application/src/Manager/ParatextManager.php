<?php

namespace App\Manager;

use App\Entity\Paratext;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

class ParatextManager
{
    public function __construct(EntityManagerInterface $em, ParameterBagInterface $params)
    {
        $this->em = $em;
        $this->params = $params;
        $this->dir = $this->params->get('xml_directory');
    }

    public function create($name, $author)
    {
        $paratext = new Paratext();
        $paratext->setName($name);
        $paratext->setAuthor($author);

        return $paratext;
    }

    public function deleteFile(Paratext $paratext)
    {
        $filesystem = new Filesystem();
        $filesystem->remove($this->dir.DIRECTORY_SEPARATOR.$paratext->getFilename());

        return;
    }
}
