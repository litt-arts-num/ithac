<?php

namespace App\Manager;

use App\Entity\Paratext;
use App\Manager\XmlManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;

class SearchManager
{
    private $xmlManager;
    private $em;

    public function __construct(EntityManagerInterface $em, XmlManager $xmlManager)
    {
        $this->em = $em;
        $this->xmlManager = $xmlManager;
    }

    public function search($query, $type)
    {
        $allParatexts = $this->em->getRepository(Paratext::class)->findAll();
        $paratexts = [];

        foreach ($allParatexts as $paratext) {
            $path = $this->xmlManager->getParatextPath($paratext);
            $content = file_get_contents($path);
            $untagContent = strip_tags($content);
            $hit = false;

            switch ($type) {
              case 'xpath':
                $crawler = new Crawler($content);
                $matches = $crawler->filterXPath($query);
                if ($matches->count() > 0) {
                    $hit = true;
                }
                break;
              case 'text':
                if (strpos($content, $query)) {
                    $hit = true;
                }
                if (strpos($untagContent, $query)) {
                    $hit = true;
                }
                break;
              case 'regexp':
                $isRegexp = $this->isRegularExpression($query);
                if ($isRegexp && preg_match($query, $content)) {
                    $hit = true;
                }
                if ($isRegexp && preg_match($query, $untagContent)) {
                    $hit = true;
                }
                break;
            }

            if ($hit) {
                $paratexts[] = $paratext;
            }
        }

        return $paratexts;
    }

    private function isRegularExpression($string)
    {
        return @preg_match($string, '') !== false;
    }
}
