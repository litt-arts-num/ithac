<?php

namespace App\Manager;

use App\Entity\Edition;
use Doctrine\ORM\EntityManagerInterface;

class EditionManager
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create($name)
    {
        $edition = new Edition();
        $edition->setName($name);

        return $edition;
    }

    public function cleanEmpty()
    {
        $emptyEditions = $this->em->getRepository(Edition::class)->findEmpty();
        foreach ($emptyEditions as $emptyEdition) {
            $this->em->remove($emptyEdition);
        }
        $this->em->flush();
        
        return;
    }
}
