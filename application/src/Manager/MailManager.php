<?php

namespace App\Manager;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MailManager
{
    private $mailer;
    private $templating;
    private $params;

    public function __construct(ParameterBagInterface $params, \Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->params = $params;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendGenericMail()
    {
        $body = $this->templating->render(
          'mail/mail.html.twig',
          ['body' => "contenu test"]
        );

        $to = "";
        $subject= "test template";

        $this->send($to, $subject, $body);
    }

    public function send($to, $subject, $body)
    {
        $message = (new \Swift_Message($subject))
          ->setContentType('text/html')
          ->setFrom($this->params->get('platform_email'))
          ->setTo($to)
          ->setBody($body);


        $this->mailer->send($message);

        return;
    }
}
