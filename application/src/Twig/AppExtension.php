<?php
namespace App\Twig;

use App\Entity\Parameters;
use App\Entity\Paratext;
use App\Manager\CacheManager;
use App\Manager\XmlManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $em;
    private $cm;
    private $xm;

    public function __construct(EntityManagerInterface $em, CacheManager $cm, XmlManager $xm)
    {
        $this->em = $em;
        $this->cm = $cm;
        $this->xm = $xm;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getParameters', [$this, 'getParameters']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('paratextToHtml', [$this, 'paratextToHtml']),
            new TwigFilter('publicParatextToHtml', [$this, 'publicParatextToHtml']),
        ];
    }

    public function getParameters()
    {
        $parametersCache = $this->cm->get("parameters");

        if ($parametersCache->isHit()) {
            $parameters = $parametersCache->get();
        } else {
            $parameters = $this->em->getRepository(Parameters::class)->findOneBy([]);
            $this->cm->store($parametersCache, $parameters);
        }

        return $parameters;
    }

    public function paratextToHtml(Paratext $paratext)
    {
        return $this->xm->getHTML($paratext);
    }

    public function publicParatextToHtml(Paratext $paratext)
    {
        return $this->xm->getPublicHTML($paratext);
    }
}
