<?php

namespace App\Controller;

use App\Entity\Paratext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class VisualisationController extends AbstractController
{
    /**
     * @Route("/explorer", name="explorer")
     */
    public function explorer()
    {
        return $this->render('visualisation/explorer.html.twig');
    }

    /**
     * @Route("/api/paratexts", name="paratexts", options={"expose"=true})
     */
    public function paratexts()
    {
        $em = $this->getDoctrine()->getManager();
        $paratexts = $em->getRepository(Paratext::class)->findAll();
        $paratextsFiles = [];
        foreach ($paratexts as $paratext) {
            $paratextsFiles[] = $paratext->getFilename();
        }

        $response = new JsonResponse($paratextsFiles);

        return $response;
    }

    /**
     * @Route("/api/paratexts-extended", name="paratexts-extended", options={"expose"=true})
     */
    public function paratextsExtended()
    {
        $em = $this->getDoctrine()->getManager();
        $paratexts = $em->getRepository(Paratext::class)->findAll();
        $paratextsFiles = [];
        foreach ($paratexts as $paratext) {

            $xml = file_get_contents("upload/tei/" . $paratext->getFilename());

            $crawler = new Crawler($xml);

            $language = $crawler->filterXPath('//TEI/teiHeader/fileDesc/titleStmt/title/@xml:lang');
            $pubPlace = $crawler->filterXPath('//TEI/teiHeader/fileDesc/sourceDesc/bibl/pubPlace');
            $anas = $crawler->filterXPath('//*/@ana');
            $pubPlace = $pubPlace->count() > 0 ? $pubPlace->text() : "";
            $language = $language->count() > 0 ? $language->text() : "";
            $anas = ($anas->count() > 0) ? $this->getDistinctAnas($anas) : [];

            $p["id"] = $paratext->getId();
            $p["filename"] = $paratext->getOriginalFilename();
            $p["title"] = preg_replace("/<br>|\n/", "", $paratext->getName());
            $p["location"] = $pubPlace;
            $p["lang"] = $language;
            $p["date"] = $paratext->getEdition()->getName();
            $p["author"] = $paratext->getAuthor();
            $p["semantic"] = $anas;

            $paratextsFiles[] = $p;
        }

        $response = new JsonResponse($paratextsFiles);

        return $response;
    }

    private function getDistinctAnas($anaEl)
    {
        $out = [];
        $anaEl->each(function (Crawler $el) use (&$out) {
            $anas = explode(" ", $el->text());
            foreach ($anas as $ana) {
                if (!in_array($ana, $out) && $ana != "")  $out[] = $ana;
            }
        });

        return $out;
    }

    /**
     * @Route("/api/paratext/{filename}", name="paratext", options={"expose"=true})
     */
    public function paratext($filename)
    {
        $dir = $this->getParameter('xml_directory');
        $fileContent = file_get_contents($dir . DIRECTORY_SEPARATOR . $filename);

        return new Response($fileContent);
    }
}
