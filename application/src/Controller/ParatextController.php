<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Paratext;
use App\Form\MessageType;
use App\Form\QueryType;
use App\Manager\ParatextManager;
use App\Manager\SearchManager;
use App\Manager\XmlManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/** @Route(name="paratext_") */
class ParatextController extends AbstractController
{
    private $paratextManager;
    private $searchManager;
    private $xmlManager;

    public function __construct(ParatextManager $paratextManager, SearchManager $searchManager, XmlManager $xmlManager)
    {
        $this->paratextManager = $paratextManager;
        $this->searchManager = $searchManager;
        $this->xmlManager = $xmlManager;
    }

    /**
     * @Route("/admin/search", name="search")
     */
    public function search(Request $request)
    {
        $form = $this->createForm(QueryType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $query = $form->get('query_string')->getData();
            $type = $form->get('type')->getData();
            $paratexts = $this->searchManager->search($query, $type);

            if (!empty($paratexts)) {
                return $this->render('paratext/list.html.twig', [
                    "paratexts" => $paratexts,
                    "query" => $query,
                    "type" => $type
                ]);
            }
        }

        return $this->render('paratext/search.html.twig', [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/paratext/{originalFilename}", name="public_display")
     * @Route("/p/{originalFilename}", name="public_display_short")
     */
    public function publicDisplay(Paratext $paratext)
    {
        return $this->render(
            'paratext/public-display.html.twig',
            ['paratext' => $paratext]
        );
    }

    /**
     * @Route("/admin/paratext/{originalFilename}", name="display")
     */
    public function display(Paratext $paratext)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(MessageType::class, null, [
            'action' => $this->generateUrl('message_post', ['id' => $paratext->getId()]),
        ]);
        $generalComments = $em->getRepository(Message::class)->findBy(["paratext" => $paratext, "target" => null]);

        return $this->render(
            'paratext/display.html.twig',
            [
                'paratext' => $paratext,
                'form' => $form->createView(),
                'annotations' => $paratext->getMessages(),
                'generalComments' => $generalComments
            ]
        );
    }

    /**
     * @Route("/admin/paratext/{originalFilename}/odt", name="odt")
     */
    public function getOdt(Paratext $paratext)
    {
        $source = '/var/www/data/xsl/tei2odt/odt_model';
        $destination = $paratext->getOriginalFilename() . ".odt";

        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }
        if (file_exists($destination)) {
            unlink($destination);
        }

        $zip = new \ZipArchive;

        if (!$zip->open($destination, \ZipArchive::CREATE)) {
            return false;
        }
        if (is_dir($source)) {
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);
            foreach ($files as $file) {
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                    continue;
                $file = realpath($file);
                if (is_dir($file)) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } else if (is_file($file)) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($source)) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        $content = $this->xmlManager->getOdtContent($paratext);
        $zip->addFromString("content.xml", $content);
        $zip->close();

        $response = $this->file($destination, $destination);
        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * @Route("/admin/paratext/r/{filename}", name="redirect")
     */
    public function redirectFromFilename(Paratext $paratext)
    {
        return $this->redirectToRoute('paratext_display', ["originalFilename" => $paratext->getOriginalFilename()]);
    }

    /**
     * @Route("/admin/paratexts", name="list")
     */
    public function list()
    {
        $em = $this->getDoctrine()->getManager();
        $paratexts = $em->getRepository(Paratext::class)->findBy([], ["lastUpload" => "DESC"]);

        return $this->render('paratext/list.html.twig', ['paratexts' => $paratexts]);
    }

    /**
     * @Route("/admin/paratext/{id}/delete", name="remove")
     */
    public function delete(Paratext $paratext)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $this->paratextManager->deleteFile($paratext);
        $edition = $paratext->getEdition();

        $em = $this->getDoctrine()->getManager();
        $em->remove($paratext);
        $em->flush();

        $this->addFlash('notice', 'Paratexte supprimé');

        return $this->redirectToRoute('edition_display', ["id" => $edition->getId()]);
    }
}
