<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Edition;
use App\Entity\Parameters;
use App\Entity\Paratext;
use App\Entity\Message;
use App\Entity\User;
use App\Form\ParametersType;
use App\Form\UploadType;
use App\Manager\AuthorManager;
use App\Manager\CacheManager;
use App\Manager\EditionManager;
use App\Manager\ParatextManager;
use App\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/super-admin", name="admin_")
 */
class AdminController extends AbstractController
{
    public function __construct(CacheManager $cm, UserManager $um, AuthorManager $authorManager, EditionManager $editionManager, ParatextManager $paratextManager)
    {
        $this->cm = $cm;
        $this->um = $um;
        $this->authorManager = $authorManager;
        $this->editionManager = $editionManager;
        $this->paratextManager = $paratextManager;
    }

    /**
   * @Route("/upload-xml", name="upload_xml")
   */
    public function importXml(Request $request)
    {
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $xmlDir = $this->getParameter('xml_directory');
            $xmlFiles = $form->get('xml')->getData();
            $onlyOneXml = (count($xmlFiles) == 1) ? true : false;

            foreach ($xmlFiles as $xmlFile) {
              //foreach file
              $notEnoughData = false;
              $originalFilename = pathinfo($xmlFile->getClientOriginalName(), PATHINFO_FILENAME);
              $paratext = $em->getRepository(Paratext::class)->findOneByOriginalFilename($originalFilename);

              $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
              $xml = file_get_contents($xmlFile->getPathname());

              $crawler = new Crawler($xml);

              $authorName = $crawler->filterXPath('//TEI/teiHeader/fileDesc/sourceDesc/bibl/author');
              $editionName = $crawler->filterXPath('//TEI/teiHeader/fileDesc/sourceDesc/bibl/date/@when');
              $paratextName = $crawler->filterXPath('//TEI/teiHeader/fileDesc/titleStmt/title');
              $paratextAuthor = $crawler->filterXPath('//TEI/teiHeader/fileDesc/titleStmt/author');

              // Gestion de l'auteur
              if ($authorName->count() > 0) {
                $authorName = $authorName->text();
                if (!$author = $em->getRepository(Author::class)->findOneByName($authorName)) {
                  $author = $this->authorManager->create($authorName);
                }
              } else {
                $notEnoughData = true;
                $this->addFlash('warning', "Problème avec le nom de l'auteur : " . $originalFilename);
              }

              // Gestion de l'édition
              if ($editionName->count() > 0) {
                $editionName = $editionName->text();
                if (!$notEnoughData && !$edition = $em->getRepository(Edition::class)->findOneBy(["author" => $author, "name" => $editionName])) {
                  $edition = $this->editionManager->create($editionName);
                  $author->addEdition($edition);
                }
              } else {
                $notEnoughData = true;
                $this->addFlash('warning', "Problème avec le nom de l'édition : " . $originalFilename);
              }

              // Gestion du paratexte
              if ($paratextName->count() > 0 && $paratextAuthor->count() > 0) {
                $paratextName = $paratextName->text();
                $paratextAuthor = $paratextAuthor->text();
                if (!$notEnoughData) {
                  if (!$paratext) {
                    $paratext = $this->paratextManager->create($paratextName, $paratextAuthor);
                  } else {
                    $this->paratextManager->deleteFile($paratext);
                  }
                  $edition->addParatext($paratext);
                  $paratext->setAuthor($paratextAuthor);
                  $paratext->setName($paratextName);
                }
              } else {
                $notEnoughData = true;
                $this->addFlash('warning', "Problème avec le nom du paratexte ou l'auteur du paratexte : " . $originalFilename);
              }

              if ($notEnoughData) {
                  if ($onlyOneXml) {
                    return $this->redirectToRoute('admin_upload_xml');
                  }
                  continue;
              }

              // Déplacement du fichier
              $newFilename = $safeFilename.'-'.uniqid().'.xml';
              try {
                $xmlFile->move($xmlDir, $newFilename);
              } catch (FileException $e) {
                $this->addFlash('warning', "Erreur au déplacement du fichier : " . $originalFilename);
                if ($onlyOneXml) {
                  $this->redirectToRoute('admin_upload_xml');
                }
              }
              $paratext->setFilename($newFilename);
              $paratext->setOriginalFilename($originalFilename);
              $paratext->setLastUpload(new \DateTime());

              $this->authorManager->save($author);

              if ($onlyOneXml) {
                $this->addFlash('notice', "Chargement XML ok : " . $originalFilename);
              }
            }

            $this->editionManager->cleanEmpty();
            $this->authorManager->cleanEmpty();

            if (!$onlyOneXml) {
              $this->addFlash('notice', "Chargement XMLs ok");
            }

            if ($onlyOneXml) {
              return $this->redirectToRoute('paratext_display', ["originalFilename" => $paratext->getOriginalFilename()]);
            } else {
              return $this->redirectToRoute('paratext_list');
            }
        } else {
            return $this->render('admin/upload-xml.html.twig', [
              'form' => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function index()
    {
        $users = $this->um->getUsers();

        return $this->render('admin/users.html.twig', [
          "users" => $users
        ]);
    }

    /**
     * @Route("/user/role/{id}/{role}", name="set_role")
     */
    public function setRole(User $user, $role)
    {
        $this->um->setRole($user, [$role]);
        $this->addFlash('success', "switch admin done");

        return $this->redirectToRoute('admin_list_users');
    }

    /**
     * @Route("/comments", name="comments")
     */
    public function listComments()
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository(Message::class)->findBy([], ["creationDate" => "DESC"], 200);

        return $this->render('admin/comments.html.twig', [
          'comments' => $comments,
        ]);
    }

    /**
     * @Route("/parameters", name="parameters")
     */
    public function parameters(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if ($parameters = $em->getRepository(Parameters::class)->findOneBy([])) {
            $form = $this->createForm(ParametersType::class, $parameters);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $request = $request->request;
                $em->persist($parameters);
                $em->flush();
                $this->cm->clear("parameters");

                $this->addFlash('notice', 'Paramètres modifiés');

                return $this->redirectToRoute('homepage');
            }

            return $this->render('admin/parameters.html.twig', [
              "form" => $form->createView()
            ]);
        }

        return $this->redirectToRoute('homepage');
    }
}
