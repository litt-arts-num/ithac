<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Author;

/** @Route("/admin", name="author_") */
class AuthorController extends AbstractController
{
    /**
     * @Route("/authors", name="index")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $authors = $em->getRepository(Author::class)->findAll();

        return $this->render('author/index.html.twig', ['authors' => $authors]);
    }

    /**
     * @Route("/author/{id}", name="display")
     */
    public function display(Author $author)
    {
        return $this->render('author/display.html.twig', ['author' => $author]);
    }

    /**
     * @Route("/author/{id}/delete", name="remove")
     */
    public function delete(Author $author)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $em->remove($author);
        $em->flush();

        $this->addFlash('notice', 'Auteur supprimé');

        return $this->redirectToRoute('author_index');
    }
}
