<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Edition;

/** @Route("/admin", name="edition_") */
class EditionController extends AbstractController
{
    /**
     * @Route("/edition/{id}", name="display")
     */
    public function display(Edition $edition)
    {
        return $this->render('edition/display.html.twig', ['edition' => $edition]);
    }

    /**
     * @Route("/edition/{id}/delete", name="remove")
     */
    public function delete(Edition $edition)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $author = $edition->getAuthor();

        $em = $this->getDoctrine()->getManager();
        $em->remove($edition);
        $em->flush();

        $this->addFlash('notice', 'Edition supprimée');

        return $this->redirectToRoute('author_display', ["id" => $author->getId()]);
    }
}
