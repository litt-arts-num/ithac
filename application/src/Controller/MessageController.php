<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Paratext;
use App\Form\MessageType;
use App\Manager\MessageManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/** @Route("/admin", name="message_") */
class MessageController extends AbstractController
{
    public function __construct(MessageManager $messageManager)
    {
        $this->messageManager = $messageManager;
    }

    /**
     * @Route("/annotations/{id}", name="annotations", options={"expose"=true})
     */
    public function displayAnnotations(Paratext $paratext, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $target = $request->request->get('target');
        $annotations = $em->getRepository(Message::class)->findBy(["paratext" => $paratext, "target" => $target], ["creationDate" => "ASC"]);
        $form = $this->createForm(MessageType::class, null, [
          'action' => $this->generateUrl('message_post', ['id' => $paratext->getId()]),
          'data_target' =>  $target
        ]);

        return $this->render('paratext/comments.html.twig', ['form' => $form->createView(), 'annotations' => $annotations, 'partial' => true]);
    }


    /**
     * @Route("/edit/{id}", name="edit", options={"expose"=true})
     */
    public function editAnnotation(Message $message, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $target = $message->getTarget();
        $paratext = $message->getParatext();
        $form = $this->createForm(MessageType::class, $message, [
          'action' => $this->generateUrl('message_post', ['id' => $paratext->getId(), 'messageId' => $message->getId()]),
          'data_target' =>  $target,
          'data_edit' =>  true
        ]);

        return $this->render('paratext/comment-edit.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @Route("/annotation/{id}/remove", name="annotation_remove", options={"expose"=true})
     */
    public function removeAnnotation(Message $message)
    {
        $paratext = $message->getParatext();
        $this->messageManager->remove($message);
        $this->addFlash('notice', "Commentaire supprimé");

        return $this->redirectToRoute('paratext_display', ["originalFilename" => $paratext->getOriginalFilename()]);
    }

    /**
     * @Route("/message/post/{id}/{messageId}", name="post", defaults={"messageId" = null})
     * @ParamConverter("message", class="App\Entity\Message", options={"id" = "messageId"})
     */
    public function post(Paratext $paratext, ?Message $message, Request $request)
    {
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $form->getData();
            $msg = $this->messageManager->create($message, $paratext);
            $this->addFlash('notice', "commentaire envoyé");
        }

        return $this->redirectToRoute('paratext_display', ["originalFilename" => $paratext->getOriginalFilename(), '_fragment' => $msg->getTarget()]);
    }
}
