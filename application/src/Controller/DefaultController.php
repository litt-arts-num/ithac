<?php

namespace App\Controller;

use App\Manager\MailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        // $this->addFlash('notice', 'Toast flashmessages');

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/manual", name="manual")
     */
    public function manual()
    {
        return $this->render('editorial/manual.html.twig');
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }

    /**
     * @Route("/test-mail", name="test_mail")
     */
    public function mail(MailManager $mm)
    {
        $mm->sendGenericMail();

        return $this->render('default/index.html.twig');
    }
}
