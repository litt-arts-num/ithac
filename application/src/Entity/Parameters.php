<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParametersRepository")
 */
class Parameters
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $trackingCode;

    /**
     * @ORM\Column(type="boolean")
     */
    private $userBottom;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }

    public function setTrackingCode(?string $trackingCode): self
    {
        $this->trackingCode = $trackingCode;

        return $this;
    }

    public function getUserBottom(): ?bool
    {
        return $this->userBottom;
    }

    public function setUserBottom(bool $userBottom): self
    {
        $this->userBottom = $userBottom;

        return $this;
    }
}
