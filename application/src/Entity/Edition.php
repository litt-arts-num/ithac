<?php

namespace App\Entity;

use App\Repository\EditionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EditionRepository::class)
 */
class Edition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Paratext::class, mappedBy="edition", cascade={"persist", "remove"})
     * @ORM\OrderBy({"originalFilename" = "ASC"})
     */
    private $paratexts;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="editions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    public function __construct()
    {
        $this->paratexts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Paratext[]
     */
    public function getParatexts(): Collection
    {
        return $this->paratexts;
    }

    public function addParatext(Paratext $paratext): self
    {
        if (!$this->paratexts->contains($paratext)) {
            $this->paratexts[] = $paratext;
            $paratext->setEdition($this);
        }

        return $this;
    }

    public function removeParatext(Paratext $paratext): self
    {
        if ($this->paratexts->contains($paratext)) {
            $this->paratexts->removeElement($paratext);
            // set the owning side to null (unless already changed)
            if ($paratext->getEdition() === $this) {
                $paratext->setEdition(null);
            }
        }

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }
}
