<?php

namespace App\Command;

use App\Entity\Parameters;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FixturesCommand extends Command
{
    private $em;
    protected static $defaultName = 'app:fixtures';

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Add base datas to application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('fixtures');
        // parameters
        if (!$parameters = $this->em->getRepository(Parameters::class)->findOneBy([])) {
            $parameters = new Parameters;
            $parameters->setUserBottom(false);
            $this->em->persist($parameters);
            $this->em->flush();

            $io->note('... parameters ok');
        } else {
            $io->note('... parameters already there');
        }

        $io->note('fixtures done.');
    }
}
