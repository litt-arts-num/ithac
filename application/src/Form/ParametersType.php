<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ParametersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('trackingCode', TextareaType::class, [
            'label' => "trackingCode",
            'required' => false
          ])
          ->add('userBottom', CheckboxType::class, [
            'label' => "user_bottom",
            'required' => false
          ])
          ->add('submit', SubmitType::class, array(
              'label' => 'save',
          ));
    }
}
