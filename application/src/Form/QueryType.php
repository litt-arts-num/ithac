<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class QueryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('query_string', TextType::class, [
            'label' => "Requête",
            'required' => true
          ])
          ->add('type', ChoiceType::class, [
            'required' => true,
            'label' => "Type",
            'choices'  => [
                'Plein texte' => 'text',
                'Xpath' => 'xpath',
                'Expression régulière' => 'regexp',
            ]
          ])
          ->add('submit', SubmitType::class, [
              'label' => 'Rechercher',
          ]);
    }
}
