<?php

namespace App\Form;

use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('target', HiddenType::class, [
              'data' => $options['data_target'],
            ])
            ->add('content', TextareaType::class, [
              'label' => $options['data_edit'] ? 'Edition du commentaire': 'Nouveau commentaire',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'send',
                'attr' => [
                  'class' => 'btn btn-sm btn-outline-secondary float-right'
                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Message::class,
            'data_target' => null,
            'data_edit' => false
        ]);
    }
}
