#  Laboratoire numérique pour le projet IThAC
## A propos
* https://ithac.hypotheses.org/
* https://www.hisoma.mom.fr/recherche-et-activites/projets-finances/projet-ithac
* https://cahier.hypotheses.org/ithac
* https://litt-arts.univ-grenoble-alpes.fr/le-projet-ithac-laureat-d-une-anr-600217.kjsp

## Installation

* cloner le projet

>```git clone git@gitlab.com:litt-arts-num/ithac.git```

* se placer dans le répertoire

>```cd ithac```

* lancer `make init` (en sudo ou pas selon si votre utilisateur est dans le groupe docker)

>``` make init```

* le premier fichier qui sera proposé à l'édition sera le .env dont se servira docker-compose. Il faut définir les ports d'apache et adminer, ainsi que des variables concernant mysql
* le second fichier est application/.env propre à symfony. Il faut y répercuter (voir ligne `DATABASE_URL`) les variables entrées dans le 1er fichier.
* ensuite se lance le build des images docker
* enfin, l'application symfony s'initialise.


L'application devrait être disponible sur `localhost:XXX` où `XXX` est le port défini pour apache.

* créer un compte
>```http://localhost:XXX/register```


* lui donner les droits   depuis le container apache. On s'y connecte via `docker-compose exec apache bash`

> `console app:toggleadmin <mail> ROLE_SUPER_ADMIN`

## Mise à jour
   ```
  git pull origin master
  docker-compose exec apache make update
  ```
  voir dans `application/Makefile` pour savoir tout ce que ça lance
