init:
	cp .env.dist .env
	emacs .env
	cp application/.env.dist application/.env
	emacs application/.env
	docker-compose up --build -d
	docker-compose exec apache make init

update:
	git pull
	docker-compose exec apache make update
